# Currency Converter

This project was created with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.

This is a simple frontend for a currency converter written in typescript using the angular 8 framework.

We can select currencies in the drop-down list and write the amount, the result will appear in the window next to it, we can also view the history of our operations and create a notification:

![alt text](resources/main.png)

We have a page for login or registration:

![alt text](resources/login.png)


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
