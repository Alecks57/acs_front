import {Component, Input, OnInit} from '@angular/core';
import {CurrencyService} from '../shared/currency.service';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit {

    constructor(public currencyService: CurrencyService) {

    }

    ngOnInit(): void {
        this.currencyService.fetchCurrencies().subscribe();
    }

}
