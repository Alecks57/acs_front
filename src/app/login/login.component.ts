import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LoginService} from '../shared/login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(private router: Router, public loginService: LoginService) { }

    ngOnInit() {

    }

    onLoginClick() {
        this.loginService.login()
            .subscribe(
                response => {
                    this.loginService.cookieService.set('jwt_token', response.token);
                    this.router.navigate(['/header']);
                });
    }

    onRegisterClick() {
        this.loginService.register()
            .subscribe(() => this.router.navigate(['/login']));
    }
}
