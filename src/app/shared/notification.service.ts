import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Injectable({providedIn: 'root'})
export class NotificationService {
    currency: string;
    condition: number;
    message: string;

    constructor(private http: HttpClient, public cookieService: CookieService) {
    }

    create() {
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .set('Access-Control-Allow-Origin', '*')
                // .set('Access-Control-Allow-Methods', 'POST')
                // .set('Access-Control-Allow-Headers', 'Content-Type, Authorization')
                .set('Access-Control-Allow-Credentials', 'include')
                .set('Authorization', this.cookieService.get('jwt_token'))
        };
        console.log(`http://localhost:9066/api/v1/notification/${this.currency}/${this.condition}/${this.message}`);
        return this.http.get<any>(`http://localhost:9066/api/v1/notification/${this.currency}/${this.condition}/${this.message}`, header);
    }
}
