import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';


export interface History {
    fromVal: string;
    toVal: string;
    amount: number;
    sum: number;
    date: string;
}

@Injectable({providedIn: 'root'})
export class HistoryService {
    public histories: History[] = [];

    constructor(private http: HttpClient, private cookieService: CookieService) {
    }

    fetchHistory(): void{
        this.histories = [];
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Access-Control-Allow-Origin', '*')
                .set('Accept', 'application/json')
                .set('Access-Control-Allow-Credentials', 'include')
                .set('Authorization', this.cookieService.get('jwt_token'))
        };

        this.http.get<History[]>('http://localhost:9066/api/v1/history', header)
            .subscribe(history => history.forEach(
                hist =>
                    this.histories.push(
                        {
                            fromVal: hist.fromVal,
                            toVal: hist.toVal,
                            amount: hist.amount,
                            sum: hist.sum,
                            date: hist.date
                        }
                    )
                )
            );
        this.histories.reverse();
    }


}
