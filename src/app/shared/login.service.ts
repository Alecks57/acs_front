import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

export interface History {
    fromVal: string;
    toVal: string;
    amount: number;
    sum: number;
    date: string;
}

@Injectable({providedIn: 'root'})
export class LoginService {
    public email: string;
    public pass: string;
    public token: string;

    constructor(private http: HttpClient, public cookieService: CookieService) {
    }

    login() {
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .set('Access-Control-Allow-Origin', '*')
                // .set('Access-Control-Allow-Methods', 'POST')
                // .set('Access-Control-Allow-Headers', 'Content-Type, Authorization')
                .set('Access-Control-Allow-Credentials', 'include')
        };

        const body = { email: this.email, password: this.pass };
        // this.http.post<any>('http://localhost:9066/login', body).subscribe(
        //     response => this.token = response['token']
        // );
        return this.http.post<any>('http://localhost:9066/login', body, header);
    }

    register() {
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Access-Control-Allow-Origin', '*')
                .set('Access-Control-Allow-Methods', 'POST')
                // .set('Access-Control-Allow-Credentials', 'true')
        };

        const body = { email: this.email, password: this.pass };
        return this.http.post<any>('http://localhost:9066/api/v1/registration', body, header);
    }
}
