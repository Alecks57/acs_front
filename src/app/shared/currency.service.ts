import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {LoginService} from './login.service';
import {CookieService} from 'ngx-cookie-service';


export interface Currency {
    value: string;
}


@Injectable({providedIn: 'root'})
export class CurrencyService {
    public gross: number;
    public amount: number;
    public currencies: Currency[];

    currencyFrom: string;
    currencyTo: string;

    constructor(public http: HttpClient, private cookieService: CookieService) {
        this.fetchCurrencies();
    }

    fetchCurrencies(): Observable<Currency[]> {
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Access-Control-Allow-Origin', '*')
                .set('Authorization', this.cookieService.get('jwt_token'))
        };

        return this.http.get<Currency[]>('http://localhost:9066/api/v1/converter/get/currencies', header)
            .pipe(tap(currencies => this.currencies = currencies));
    }

    getConvertedGross(value1: string, value2: string, amount: number): void {
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Access-Control-Allow-Origin', '*')
                .set('Authorization', this.cookieService.get('jwt_token'))
        };

        this.http.get<number>(`http://localhost:9066/api/v1/converter/${value1}/to/${value2}/${amount}`, header)
            .subscribe(amount => this.gross = amount['sum']);
    }

    // tslint:disable-next-line:typedef
    onChangeFrom(curr: string) {
        this.currencyFrom = curr;
    }

    // tslint:disable-next-line:typedef
    onChangeTo(curr: string) {
        this.currencyTo = curr;
    }

    // tslint:disable-next-line:typedef
    convert() {
        if (this.currencyFrom != null) {
            console.log('from ' + this.currencyFrom + ' to ' + this.currencyTo);
            this.getConvertedGross(this.currencyFrom, this.currencyTo, this.amount);
        }
    }
}
