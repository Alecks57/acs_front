import { Component, OnInit } from '@angular/core';
import {HistoryService} from '../shared/history.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
    showHistory = false;

    constructor(public historyService: HistoryService) { }

    ngOnInit(): void {
    }

    show() {
        if (!this.showHistory) {
            this.historyService.fetchHistory();

        }
        this.showHistory = !this.showHistory;
    }
}
